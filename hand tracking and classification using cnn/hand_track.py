#!/usr/bin/env python
import cv2
import numpy as np 
import tensorflow as tf 
import time


categories=['rock','paper','scissors']
all_cat = categories + ["shoot"]
def pc_decision():
    return np.random.choice(categories)


#loading the saved model
model = tf.keras.models.load_model('my_model4.h5')

hand_cascade = cv2.CascadeClassifier("cascade2.xml")

IMG_SIZE = 100
font = cv2.FONT_HERSHEY_SIMPLEX
cap = cv2.VideoCapture(0)
j=0
t=0
p="Not yet"
pp=""
game=0
you=0
draw=0
verdict=""
while True:
    ret, img = cap.read()
    img = cv2.flip(img,1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    (hx,hy,hw,hh)=(0,0,40,40)
    hands = hand_cascade.detectMultiScale(gray,minSize=(300,300))
    for (hx,hy,hw,hh) in hands:
        cv2.rectangle(img,(hx,hy),(hx+hw,hy+hh),(0,255,255),1)

    ## printing text on the image
    
    if not ((hx==0) & (hy==0)):
        image=cv2.resize(gray[hy:hy+hh,hx:hx+hw],(IMG_SIZE,IMG_SIZE))
        array = np.array(image).reshape(-1,IMG_SIZE,IMG_SIZE,1)
        array=array/255.0
        i=model.predict(array)
        prediction=categories[list(i[0]).index(max(i[0]))]

        play_next=True
        
        cv2.rectangle(img,(0,0),(0+200,0+150),(0,255,0),1)
        ##################### To be done: THe verdict of the game
        if play_next==True:
            if t==35:
                p=pc_decision()
                pp=prediction
            game_prediction=p
            me=pp
            if t==35:
                if game_prediction==me:
                    verdict="draw"
                    draw+=1
                elif (game_prediction=="rock") & (me=="paper"):
                    verdict="win"
                    you+=1
                elif (game_prediction=="paper") & (me=="scissors"):
                    verdict="win"
                    you+=1             
                elif (game_prediction=="scissors") & (me=="rock"):
                    verdict="win"
                    you+=1
                elif (game_prediction=="paper") & (me=="rock"):
                    verdict="loss"
                    game+=1
                elif (game_prediction=="scissors") & (me=="paper"):
                    verdict="loss"
                    game+=1
                elif (game_prediction=="rock") & (me=="scissors"):
                    verdict="loss"
                    game+=1
            #########
            cv2.putText(img,'game', (50,90), font, 1, (0, 255, 255), 3, cv2.LINE_AA)
            cv2.putText(img,'{}'.format(game_prediction), (0+50,0+130), font, 1, (0, 255, 0), 1, cv2.LINE_AA)

        cv2.putText(img,'you = {} '.format(prediction,game_prediction), (hx,hy+hh), font, 1, (0, 255, 0), 2, cv2.LINE_AA)
        
    #fitting the image on the screen
    cv2.putText(img,'{}'.format(all_cat[j]), (50,40), font, 1, (255, 255, 255), 3, cv2.LINE_AA)

    cv2.putText(img,'Verdict', (5,385), font, 1, (255, 150, 255), 2, cv2.LINE_AA)
    cv2.putText(img,'{}'.format(verdict), (135,385), font, 1, (255, 255, 150), 2, cv2.LINE_AA)

    cv2.putText(img,'draw', (5,415), font, 1, (255, 150, 255), 2, cv2.LINE_AA)
    cv2.putText(img,'{}'.format(draw), (135,415), font, 1, (255, 255, 150), 2, cv2.LINE_AA)

    cv2.putText(img,'you'.format(you), (5,445), font, 1, (255, 150, 255), 2, cv2.LINE_AA)
    cv2.putText(img,'{}'.format(you), (135,445), font, 1, (255, 255, 150), 2, cv2.LINE_AA)

    cv2.putText(img,'game'.format(game), (5,475), font, 1, (255, 150, 255), 2, cv2.LINE_AA)
    cv2.putText(img,'{}'.format(game), (135,475), font, 1, (255, 255, 150), 2, cv2.LINE_AA)
    img = cv2.resize(img, (1200, 800))

    if t==10:
        j+=1
    elif t==20:
        j+=1
    elif t==35:
        j+=1
    elif t==80:
        j=0
        t=0
    t+=1
    cv2.imshow('img',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()