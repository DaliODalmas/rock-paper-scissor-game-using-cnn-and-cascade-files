import shutil
import os
import cv2
import random
import numpy as np
import copy
def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles 
 
def resize_image(elem,link):
    img=cv2.imread(elem,cv2.IMREAD_GRAYSCALE)
    resized_image=cv2.resize(img,(100,100))
    cv2.imwrite(link,resized_image)
    return None

def main():
    dirName = '/home/dalmas/Documents/youtube/rock-paper-scisor/rock-paper-scissors-dataset'  
    listOfFiles = getListOfFiles(dirName) # Get the list of all files in directory tree at given path
    # copy the positive files
    f=0
    for elem in listOfFiles:
        print(elem)
        link='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/positive/{}.png'.format(f)
        resize_image(elem,link)
        f+=1
        

    dirName2 = '/home/dalmas/Documents/youtube/rock-paper-scisor/natural-images'
    listOfFiles2 = getListOfFiles(dirName2)
    # copy the negative files
    g=0
    random.shuffle(listOfFiles2)
    for elem in listOfFiles2:
        print(elem)
        if g<=(f):
            link='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/negativesforsuperimposing/{}.jpg'.format(g)
            resize_image(elem,link)
        elif g<=(f*2):
            link='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/negative/{}.jpg'.format(g)
            resize_image(elem,link)
        g+=1
        

def create_pos_neg():
    negative='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/negative'
    positive='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/superimposedpositives'
    for file_type in [negative,positive]:
        for img in os.listdir(file_type):
            if file_type==negative:
                line='negative'+'/'+img+'\n'
                with open('bg.txt','a') as f:
                    f.write(line)

            elif file_type == positive:
                line = 'superimposedpositives/'+img+' 1 0 0 100 100\n'
                with open('info.txt','a') as f:
                    f.write(line)
        
def superimpose():
    for f in range(len(os.listdir('positive'))):
        print(f,end=' ')
        imgFront = cv2.imread('./positive/{}.png'.format(f))
        img=copy.copy(imgFront)
        imgBack = cv2.imread('./negativesforsuperimposing/{}.jpg'.format(f))

        height, width = imgFront.shape[:2]


        for i in range(width):
            for j in range(height):
                pixel = imgFront[j][i]
                if np.all(pixel >= [230, 230, 230]):
                    imgFront[j][i] = imgBack[j][i]
        link='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/superimposedpositives/{}.png'.format(f)
        cv2.imwrite(link,imgFront)
        link='/home/dalmas/Documents/youtube/rock-paper-scisor/training a haar file for hand recognition/superimposedpositives/{}_{}.png'.format(f,f)
        cv2.imwrite(link,img)

if __name__ == '__main__':
    main()
    superimpose()
    create_pos_neg()