import cv2
import numpy as np

imgFront = cv2.imread('2.png')
imgBack = cv2.imread('42.jpg')
print(imgBack.shape,imgFront.shape)

height, width = imgFront.shape[:2]

#resizeBack = cv2.resize(imgBack, (width, height), interpolation = cv2.INTER_CUBIC)

for i in range(width):
    for j in range(height):
        pixel = imgFront[j][i]
        if np.all(pixel >= [230, 230, 230]):
            imgFront[j][i] = imgBack[j][i]
            print(imgFront[j][i])

cv2.imshow('image', imgFront)
cv2.waitKey(0)